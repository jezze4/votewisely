from bs4 import BeautifulSoup as bs
import json
import requests
import re
import table_api as sql

table_name = "congress_members"
table_schema = {
    "id": ("char(10)", "not null"),
    "title": ("char(30)", "not null"),
    "short_title": ("char(10)", "not null"),
    "first_name": ("char(20)", "not null"),
    "last_name": ("char(20)", "not null"),
    "full_name": ("char(40)", "not null"),
    "date_of_birth": ("char(20)", "not null"),
    "party": ("char(10)", "not null"),
    "twitter_account": ("char(64)", "null"),
    "facebook_account": ("char(64)", "null"),
    "youtube_account": ("char(64)", "null"),
    "votesmart_id": ("char(10)", "null"),
    "url": ("char(128)", "null"),
    "contact_form": ("char(128)", "null"),
    "total_votes": ("int", "null"),
    "missed_votes": ("int", "null"),
    "office": ("char(64)", "null"),
    "phone": ("char(16)", "null"),
    "state": ("char(4)", "not null"),
    "img_url": ("char(255)", "null"),
    "wiki_url": ("char(255)", "null"),
    "gender": ("char(10)", "not null"),
    "state_fullname": ("char(20)", "null"),
    "bio": ("mediumtext", "null"),
}
primary_key = "id"
foreign_keys = {}
table_constraint = {}


def get_request(url):
    r = requests.get(url)
    if r.status_code != 200:
        raise Exception(f"GET /tasks/ {r.status_code}")
    else:
        print(f"Request successful")
    return r.content


def update_member_bio(id, bio):
    member_dict = {"id": id, "bio": bio}
    sql.insert_table(table_name, table_schema, member_dict, primary_key)


def get_wiki_info():
    url = "https://en.wikipedia.org/wiki/114th_United_States_Congress"
    soup = bs(get_request(url), features="html.parser")
    member_tables = soup.find_all("table", class_="multicol")
    all_members = [
        li
        for member_table in member_tables
        for td in member_table.tbody.tr.find_all("td")
        for li in td.findAll("li")
    ]
    wiki_url = {}
    for entry in all_members:
        a_tags = entry.findAll("a")
        if len(a_tags) > 0:
            a_tag = a_tags[-1] if len(a_tags) != 3 else a_tags[1]
            wiki_url[a_tag.text] = a_tag["href"][6:]
    return wiki_url


def get_json_request(url, headers=None):
    """
    Returns the requested json from a URL
    :return:
    """
    res = requests.get(url=url, headers=headers)
    if res.status_code != 200:
        raise Exception(f"GET /tasks/ {res.status_code}")
    # else:
    #     print("API GET request successful!")
    return res.json()


def fetch_member_bio(wiki_info):
    """
    Gets up to 3 paragraphs from the intro of a wikipedia page.
    :return:
    """
    bios = {}
    for full_name, wiki_title in wiki_info.items():
        url = f"https://en.wikipedia.org/w/api.php?format=json&action=query&titles={wiki_title}&prop=extracts&exintro&explaintext"
        response = get_json_request(url)
        if "query" in response:
            extract = next(iter(response["query"]["pages"].values()))
        else:
            continue
        if "extract" in extract:
            extract = extract["extract"]
        else:
            continue
        bios[full_name] = "\n".join(extract.replace('(; ', '(').split("\n")[:3])
        print(f"Queried {full_name}")
    return bios


def insert_member_bio():
    wiki_info = get_wiki_info()
    bios = fetch_member_bio(wiki_info)
    members = sql.sql_fetch_all(f"SELECT {primary_key}, full_name FROM {table_name}")
    for id, full_name in members:
        if full_name in bios:
            print(f"Added {full_name}")
            update_member_bio(id, bios[full_name])
        else:
            print(f"Cannot find {full_name}")


if __name__ == "__main__":
    # sql.create_table(table_name, table_schema, foreign_keys, table_constraints)
    insert_member_bio()
