const express = require('express');
const helpers = require('../lib/sqlHelpers');

const router = express.Router();

const TABLE = 'bills_table_2';

function billsSQLGenerator(req) {
  const args = [];
  let sql = `${'SELECT * FROM '}${TABLE}`;
  if (typeof req.query.bill_id !== 'undefined' || typeof req.query.state !== 'undefined'
    || typeof req.query.sponsor_id !== 'undefined') {
    sql += ' WHERE ';
    if (req.query.bill_id) {
      sql += 'bill_id=? AND ';
      args.push(req.query.bill_id);
    }
    if (req.query.state) {
      sql += 'sponsor_state=? AND ';
      args.push(req.query.state);
    }
    if (req.query.sponsor_id) {
      sql += 'sponsor_id=? AND ';
      args.push(req.query.sponsor_id);
    }
    sql = sql.substring(0, sql.length - 4);
  }
  return [sql, args];
}

/**
 * Get all or some of the bill information by queries
 * /:   get all the bill information
 * /    ?bill_id="hjres158-103"
 * /    ?state="CA"
 * /    ?sponsor_id="D000435"
 */
router.get('/', helpers.sqlRequestFactory(billsSQLGenerator));

module.exports = {
  billsSQLGenerator,
  router,
};
