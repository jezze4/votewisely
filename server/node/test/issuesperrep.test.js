const chai = require('chai');

const { expect } = chai;
const sinon = require('sinon');
const issuesperrep = require('../routes/issuesperrep');

describe('IssuesPerRep', () => {
  it('issuesPerRepSQLGenerator should generate a SQL statement and its arguments', () => {
    const mockReq = sinon.mock();
    const [testSQL, testSQLargs] = issuesperrep.issuesPerRepSQLGenerator(mockReq);

    expect(testSQL).to.equal('SELECT reps_table.full_name as rep_name, COUNT(issue) as issues, reps_table.party '
    + 'FROM ' + 'reps_issue_bills' + ' '
    + 'LEFT JOIN (\
        SELECT id, full_name, party FROM ' + 'congress_members' + '\
      ) AS reps_table ON ' + 'reps_issue_bills' + '.rep_id=reps_table.id '
        + 'GROUP BY rep_id');
    expect(testSQLargs).to.be.empty;
  });
});
