const express = require('express');
const morgan = require('morgan');
const mysql = require('mysql');
const cors = require('cors');

const repsRouter = require('./routes/reps');
const stateRouter = require('./routes/states');
const issues2Router = require('./routes/issues2');
const bills2Router = require('./routes/bills2');
const statementsRouter = require('./routes/statements');
const bills2issuesRouter = require('./routes/bills2issues');
const searchRouter = require('./routes/search');
const catsPerStateRouter = require('./routes/catsperstate');
const issuesPerRepRouter = require('./routes/issuesperrep');

// / //////////////////////////////////////////////

const app = express();

const APP_PORT = process.env.PORT || 4000;
const DB_NAME = 'VoteWiselyDB';

app.use(morgan('combined'));
app.use(cors());

app.use((req, res, next) => {
  res.locals.connection = mysql.createConnection({
    host: 'cs373votewisely.cqqkkpkbowt8.us-east-1.rds.amazonaws.com',
    user: 'votewisely',
    password: 'votewisely123',
    database: DB_NAME,
    multipleStatements: true,
  });
  res.locals.connection.connect((err) => {
    if (err) {
      console.error('an error occured while connecting to Vote Wisely MySQL database');
      throw err;
    }
    console.log('connected to Vote Wisely MySQL database');
  });
  next();
});

/* Routing */
app.use('/api/reps', repsRouter.router);
app.use('/api/states', stateRouter.router);
app.use('/api/issues2', issues2Router.router);
app.use('/api/bills2', bills2Router.router);
app.use('/api/statements', statementsRouter.router);
app.use('/api/bills2issues', bills2issuesRouter.router);
app.use('/api/search', searchRouter);
app.use('/api/catsperstate', catsPerStateRouter.router);
app.use('/api/issuesperrep', issuesPerRepRouter.router);

app.listen(APP_PORT);
console.log('Node server listening to port', APP_PORT);
