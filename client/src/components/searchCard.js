import React, {PureComponent} from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import '../css/search.css';
import { Link } from 'react-router-dom';


/* Individual search card. Component within SearchResults
   4 Props:           ---------------------------------
    picture           | pic |   Title                 |
    title             |  ^  |   link                  |
    link              |  ^  |   snippet               |
    snippet           ---------------------------------
 */
export default class SearchCard extends PureComponent {

  /* Jay was messing with this */
  highlight(snippet, search) {
    /* Keywords search only */
    var terms = search.toLowerCase().split(/\s+/).sort((a, b) => b.length - a.length);
    var regex = RegExp(terms.map(w => '(?=' + w + ')').join('|'), "gi");
    /* Do not use positive lookbehinds because not all Javascript supports it yet */
    var parsedSnippet = snippet.split(regex);
    var elements = [];
    for (var i = 0; i < parsedSnippet.length; i++) {
      var sent = parsedSnippet[i];
      var term = null;
      for (var j = 0; j < terms.length && term === null; j++) {
        if (terms[j].length <= sent.length && sent.substring(0, terms[j].length).toLowerCase() === terms[j]) {
          term = terms[j];
          elements.push(<span key={'sent-' + i} className="hi-lite">{sent.substring(0, term.length)}</span>);
          elements.push(sent.substring(term.length));
        }
      }
      if (term === null) {
        elements.push(sent);
      }
    }
    return elements;
  }

  render() {
    return (
      <Container id="searchCard-container">
        <Row>
          <Col md={12}>
            <Row>
              <h4><Link to={this.props.link}>{this.props.title}</Link></h4>
            </Row>
            <Row>
              <span style={{color:"green"}}>https://thewisevote.com{this.props.link}</span>
            </Row>
            <Row>
              <p style={{color:"gray"}}>
                {this.highlight(this.props.snippet, this.props.search)}
              </p>
            </Row>
          </Col>
        </Row>
      </Container>
    );
  }
}
