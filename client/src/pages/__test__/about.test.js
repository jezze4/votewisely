import React from 'react';
import { shallow } from 'enzyme';

import MemberCard from '../../components/memberCard';
import About from '../about';

describe("About", () => {
	it("renders", () => {
		const wrapper = shallow(<About />);
	});

	it("renders with title and mission statement", () => {
		const wrapper = shallow(<About />).find("#purpose-container");
		expect(wrapper.find("h2").text()).toEqual("Our Mission");
		expect(wrapper.find("#mission-statement").exists()).toEqual(true);
	});

	it("renders with 6 people in the group", () => {
		const wrapper = shallow(<About />);
		const members = wrapper.find(MemberCard);
		expect(members.length).toEqual(6);
		expect(members.findWhere(member => member.props().name === "Jie Hao Liao").exists()).toEqual(true);
		expect(members.findWhere(member => member.props().name === "Jesus Vasquez").exists()).toEqual(true);
		expect(members.findWhere(member => member.props().name === "Jesse Martinez").exists()).toEqual(true);
		expect(members.findWhere(member => member.props().name === "Amiti Busgeeth").exists()).toEqual(true);
		expect(members.findWhere(member => member.props().name === "Lisa Barson").exists()).toEqual(true);
		expect(members.findWhere(member => member.props().name === "Fan Yang").exists()).toEqual(true);
	});
});
